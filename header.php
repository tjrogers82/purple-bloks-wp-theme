<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php wp_head(); ?>
</head>

<?php
	$addl_body_classes = array();
	$addl_body_classes[] = 'section-'.explode('/', $_SERVER['REQUEST_URI'])[1];
	if( !empty(explode('/', $_SERVER['REQUEST_URI'])[2]) && !strstr(explode('/', $_SERVER['REQUEST_URI'])[2], '?') ){
		$addl_body_classes[] = 'section-'.explode('/', $_SERVER['REQUEST_URI'])[1].'-'.explode('/', $_SERVER['REQUEST_URI'])[2];
	}

	if ( !is_front_page() ) {
		$addl_body_classes[] = 'not-front';
	}
?>

<body <?php body_class( $addl_body_classes ); ?>>

	<div class="mobile-nav">
		<?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>
		<p class="mobile-nav-close"><a href="#"><i class="fas fa-times"></i></a></p>
	</div>

	<div class="header-wrap-outer">

		<div class="header-wrap">

			<p class="site-logo"><a href="<?php echo bloginfo( 'url' ); ?>"><img src="/wp-content/themes/purplebloks/images/purple-bloks-logo.png" alt=""></a></p>

			<div class="top-nav">
				<?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>
			</div><!-- TOP NAV -->

			<p class="mobile-nav-icon"><a href="#"><i class="fas fa-bars"></i></a></p>

		</div><!-- END HEADER WRAP -->
	</div><!-- END HEADER WRAP OUTER -->

	<div class="feature-wrap-outer">

		<div class="feature-wrap">

			<div class="feature-section content">
				<p class="feature-logo-white"><img src="/wp-content/themes/purplebloks/images/purple-bloks-logo-white.png" alt="PurpleBloks"></p>

				<h1 class="feature-tagline">It's Time for a Change.</h1>
				<p class="feature-start-link"><a class="btn btn-alt" href="#">Start Today</a></p>
			</div>

			<div class="feature-section image">
				<p><img src="/wp-content/themes/purplebloks/images/purple-bloks-header-img.png" alt="Epic Battle For Change"></p>
			</div>

		</div><!-- END FEATURE WRAP -->
	</div><!-- END FEATURE WRAP OUTER -->