<?php get_header(); ?>

	<div class="contact-cta-wrap-outer">

		<div class="contact-cta-wrap">
			
			<h2 class="contact-cta-tagline">Meet new friends, <span>have some fun.</span></h2>

			<p class="contact-cta-link"><a class="btn" href="#">Contact&nbsp;Us</a></p>

		</div><?php // END CONTACT CTA WRAP ?>
	</div><?php // END CONTACT CTA WRAP OUTER ?>

	<div class="page-wrap">

		<div class="content-wrap">

			<div class="content-area">

				<div class="hp-features-section-container">
					<div class="hp-features-section">
						
						<div class="hp-feature-item trips">
							<p class="hp-feature-item-image"><img src="/wp-content/themes/purplebloks/images/take-exciting-trips.jpg" alt=""></p>
							<h3 class="hp-feature-item-title">Take Exciting Trips</h3>
						</div>

						<div class="hp-feature-item presents">
							<p class="hp-feature-item-image"><img src="/wp-content/themes/purplebloks/images/get-fun-presents.jpg" alt=""></p>
							<h3 class="hp-feature-item-title">Get Fun Presents</h3>
						</div>

						<div class="hp-feature-item friends">
							<p class="hp-feature-item-image"><img src="/wp-content/themes/purplebloks/images/meet-new-friends.jpg" alt=""></p>
							<h3 class="hp-feature-item-title">Meet New Friends</h3>
						</div>
					</div>
				</div>

			</div><?php // END CONTENT AREA ?>

		</div><?php // END CONTENT WRAP ?>

	</div><?php // END PAGE WRAP ?>

	<div class="piece-of-cake-section-container-wrap">

		<div class="piece-of-cake-section-container">
			
			<div class="piece-of-cake-section content">
				<div class="piece-of-cake-content">
					<div class="piece-of-cake-content-section image-container">
						<p class="piece-of-cake-content-image">
							<img src="/wp-content/themes/purplebloks/images/its-a-piece-of-cake.png" alt="PurpleBloks">
						</p>
					</div>

					<div class="piece-of-cake-content-section text">
						<h2>It's a Piece<br> of Cake.</h2>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna.</p>
					</div>
				</div>
			</div>

			<div class="piece-of-cake-section image">
				<p class="piece-of-cake-logo-white"><img src="/wp-content/themes/purplebloks/images/purple-bloks-logo-white.png" alt="PurpleBloks"></p>
			</div>

		</div><!-- END CAKE SECTION -->
	</div><?php // END CAKE SECTION CONTAINER ?>

<?php get_footer(); ?>