<div class="contact-form-section-container">

	<div class="contact-form-section">		
		<h2>Take The First Step. <span>Contact Us Today!</span></h2>

		<div class="contact-form">
			<input class="contact-form-field" type="text" name="name" placeholder="Name" required="required">
			<input class="contact-form-field" type="email" name="email" placeholder="Email Address" required="required">
			<input class="contact-form-submit" type="button" name="submit" value="Start Today">
		</div>
	</div><?php // END CONTACT CTA WRAP ?>
</div><?php // END CONTACT CTA WRAP OUTER ?>

<div class="footer-section-container-wrap">

	<div class="footer-section-container">

		<div class="footer-section footer-logo">
			<p class="site-logo-footer"><a href="<?php echo bloginfo( 'url' ); ?>"><img src="/wp-content/themes/purplebloks/images/purple-bloks-logo-white.png" alt="PurpleBloks"></a></p>
		</div>

		<div class="footer-section footer-nav">
			<div class="footer-nav-section learn">
				<p>Learn</p>
				<ul class="footer-nav-menu">
					<li><a href="#">Contact</a></li>
					<li><a href="#">Support</a></li>
					<li><a href="#">Meet our team</a></li>
				</ul>
			</div>

			<div class="footer-nav-section links">
				<p>Links</p>
				<ul class="footer-nav-menu">
					<li><a href="#">Products</a></li>
					<li><a href="#">Services</a></li>
				</ul>
			</div>
		</div>

	</div><!-- end of footerWrap -->

</div><!-- end of footerWrapOuter -->

<?php wp_footer(); ?>

</body>
</html>