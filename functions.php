<?php
/**
* Add theme supports
*/
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
add_theme_support( 'title-tag' );
add_post_type_support( 'page', 'excerpt' );

// add additional excerpt support by post type
// add_post_type_support( 'post_type' , 'excerpt' );

/**
Add theme support for WooCommerce (optional)
*/
// add_action( 'after_setup_theme', 'woocommerce_support' );
// function woocommerce_support() {
//     add_theme_support( 'woocommerce' );
// }

function register_theme_menus() {
	register_nav_menus(
		array(
			'primary-menu' => __( 'Primary Menu' ),
		)
	);
}
add_action( 'init', 'register_theme_menus' );

/**
* Enqueue scripts and styles.
>>> REPLACE "theme" with namespace for this theme <<<
*/
function purplebloks_styles_scripts() {

	// Theme stylesheet.
	wp_enqueue_style( 'purplebloks-main', get_theme_file_uri( '/style.css' ) );
	wp_enqueue_style( 'purplebloks-all', get_theme_file_uri( '/css/all.css' ) );
	wp_enqueue_style( 'purplebloks-googlefonts', 'https://fonts.googleapis.com/css?family=Muli:400,900&display=swap' );
	
	if( is_front_page() ){
		wp_enqueue_style( 'purplebloks-homepage', get_theme_file_uri( '/css/home.css' ) );
		wp_enqueue_style( 'purplebloks-googlefonts', 'https://fonts.googleapis.com/css?family=Muli:400,900&display=swap' );
	}

	wp_enqueue_script( 'purplebloks-fontawesome', 'https://kit.fontawesome.com/df407decf0.js' );
	//wp_enqueue_style( 'theme-animatecss', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css' );
	wp_enqueue_script( 'purplebloks-global', get_theme_file_uri( '/js/purplebloks.js' ), array('jquery') );
}
add_action( 'wp_enqueue_scripts', 'purplebloks_styles_scripts' );



/**
* Register our sidebars and widgetized areas.
*/
function create_widget($name, $id, $description) {

    register_sidebar(array(
        'name' => __( $name ),
        'id' => $id,
        'description' => __( $description ),
        'before_widget' => '<div id="'.$id.'" class="widget %1$s %2$s">',
		'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ));

}
// Create the actual widgets (ID is a unique string)
create_widget("Name", "id", "Description");


// ---------------------------------------------------------------------------------
// ----- GIVE PAGES PRIORITY OVER POSTS IN URLS ------------------------------------
// ----- https://wordpress.stackexchange.com/questions/22438/how-to-make-pages-slug-have-priority-over-any-other-taxonomies-like-custom-post
// ---------------------------------------------------------------------------------
add_action( 'init', 'purplebloks_init' );
function purplebloks_init() {
	$GLOBALS['wp_rewrite']->use_verbose_page_rules = true;
}

add_filter( 'page_rewrite_rules', 'purplebloks_collect_page_rewrite_rules' );
function purplebloks_collect_page_rewrite_rules( $page_rewrite_rules ){
	$GLOBALS['purplebloks_page_rewrite_rules'] = $page_rewrite_rules;
	return array();
}

add_filter( 'rewrite_rules_array', 'tpurplebloks_prepend_page_rewrite_rules' );
function purplebloks_prepend_page_rewrite_rules( $rewrite_rules ){
	return $GLOBALS['purplebloks_page_rewrite_rules'] + $rewrite_rules;
}